/*
 * Library:  lmfit (Levenberg-Marquardt least squares fitting)
 *
 * cc -o sigmoid -I/usr/local/include -L/usr/local/lib -llmfit -lm sigmoid.c
 * ./sigmoid
 *
 *
 * We're aiming at the inverse of a sigmoid function, which is a very nice fit
 * ... except for value around 0, voltage needs to be capped
 *
 */
 
#include "lmcurve.h"
#include <math.h>
#include <stdio.h>


/* model function: sigmoid */

double f( double v, const double *p )
{
   return p[0] / (1 + exp(p[1] * (v + 1 - p[2])));
}
 
int main()
{
    int n = 3; /* number of parameters in model function f */
    double par[3] = {  1, 1, 1 }; /* starting values */
    
    /* data points */
    int m = 10;
    int i;
    double v[10] = { 4.2, 4.0,   3.9,  3.8,  3.7,  3.6,  3.5,  3.4,  3.2,   3.0 };
    double c[10] = { 0.0, 15.0, 22.0, 40.0, 75.0, 90.0, 92.0, 95.0, 99.0, 100.0 };

    lm_control_struct control = lm_control_double;
    lm_status_struct status;
    control.verbosity = 9;
    control.patience = 1000; 
    control.epsilon = 0.001;

    printf( "Fitting ...\n" );
    /* now the call to lmfit */
    lmcurve( n, par, m, v, c, f, &control, &status );
        
    printf( "Results:\n" );
    printf( "status after %d function evaluations:\n  %s\n",
            status.nfev, lm_infmsg[status.outcome] );

    printf("obtained parameters:\n");
    for ( i = 0; i < n; ++i)
        printf("  par[%i] = %12g\n", i, par[i]);
    printf("obtained norm:\n  %12g\n", status.fnorm );
    
    printf("fitting data as follows:\n");
    for ( i = 0; i < m; ++i)
        printf( "  v[%2d]=%4g c=%6g fit=%10g residue=%12g\n",
                i, v[i], c[i], f(v[i],par), c[i] - f(v[i],par) );

    printf( "log( %6g / c - 1 ) / %6g + %6g\n", par[0], par[1], par[2] );

    return 0;
}
