/*
 * Library:  lmfit (Levenberg-Marquardt least squares fitting)
 *
 * cc -o polynomial -I/usr/local/include -L/usr/local/lib -llmfit -lm polynomial.c
 * ./polynomial
 *
 */
 
#include "lmcurve.h"
#include <math.h>
#include <stdio.h>

#define DELTA 0.0001

/* model function: a 3rd degree polynomial */

double f( double x, const double *p )
{
   return p[0] + p[1] * x + p[2] * pow(x, 2) + p[3] * pow(x,3);
}
       
 
int main()
{
    int n = 4; /* number of parameters in model function f */
    double par[4] = {  1, 1, 1, 1 }; /* starting values */
    
    /* data points */
    int m = 10;
    int i;
    double v[10] = { 4.2, 4.0,   3.9,  3.8,  3.7,  3.6,  3.5,  3.4,  3.2,   3.0 };
    double x[10] = { 0.0, 15.0, 22.0, 40.0, 75.0, 90.0, 92.0, 95.0, 99.0, 100.0 };

    lm_control_struct control = lm_control_double;
    lm_status_struct status;
    control.verbosity = 9;
    control.patience = 1000; 
    //control.epsilon = 0.001;

    printf( "Fitting ...\n" );
    /* now the call to lmfit */
    lmcurve( n, par, m, x, v, f, &control, &status );
        
    printf( "Results:\n" );
    printf( "status after %d function evaluations:\n  %s\n",
            status.nfev, lm_infmsg[status.outcome] );

    printf("obtained parameters:\n");
    for ( i = 0; i < n; ++i)
        printf("  par[%i] = %12g\n", i, par[i]);
    printf("obtained norm:\n  %12g\n", status.fnorm );
    
    printf("fitting data as follows:\n");
    for ( i = 0; i < m; ++i)
        printf( "  v[%2d]=%4g x=%6g fit=%10g residue=%12g\n",
                i, x[i], v[i], f(x[i],par), v[i] - f(x[i],par) );
    printf( "%g * x^3 + %g * x^2 + %g * x + %g\n", par[3], par[2], par[1], par[0] );

    return 0;
}
