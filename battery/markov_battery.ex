defmodule MarkovBattery do

  def transition( { q, n, t, charge, transmitted } ) do
    s = :random.seed(:erlang.now)
    {rand, _} = :random.uniform_s(s)
    if (rand <= q) do
      { q, n, t, charge - 1, transmitted + 1 }
    else
      new_charge = recover?( charge, n, transmitted, t )
      { q, n, t, new_charge, transmitted }
    end
  end

  def recover?(charge, n, _transmitted, _t) when charge == n, do: charge
  def recover?(charge, n, transmitted, t) do
    s = :random.seed(:erlang.now)
    {rand, _} = :random.uniform_s(s) 
    threshold = (charge/n) * :math.exp( -50 * transmitted/t)
    if rand <= threshold, do: charge + 1,
    else: charge
  end
end

## s = {0.3, 100000, 200000, 100000, 0}
## l = Stream.iterate(s, fn(state) -> MarkovBattery.transition(state) end) |> Enum.take(300_000)
##  y = Enum.map(l, fn({_, _, _, _, v, _}) -> v end); data = Enum.zip 0..70_000, y
##  {:ok, file} = File.open "battery.dat", [:write] ; Enum.each(data, fn({x, y}) -> IO.binwrite(file, "#{x} #{y}\n") end);  File.close file
