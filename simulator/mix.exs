defmodule Simulator.Mixfile do
  use Mix.Project

  def project do
    [app: :simulator,
     version: "0.0.1",
     deps: deps]
  end

  def application do
    [

     applications: [:logger, :inets, :ssl, :postgrex, :ecto]]
  end

  defp deps do
    [
      {:poison, git: "https://github.com/devinus/poison.git"},
      {:polyline, git: "https://github.com/dvionnet/polyline.git"},
      {:postgrex, github: "ericmj/postgrex", optional: true},
      {:ecto, github: "elixir-lang/ecto"}]
  end
end
