defmodule Forwarder do
  use GenEvent
  import Ecto.Query

  def init(agent_pid) do
    IO.inspect self
    {:ok, agent_pid}
  end

  def handle_event({:tick, timestamp}, agent_pid) do

    Agent.cast(agent_pid,
               fn({freq, next, count}) ->
                 if (timestamp == next or next == nil) do
                   q = from s in Step, where: s.ref == "#Ref<0.0.1.74810>" and s.second <= ^count, select: max(s.second)
                   IO.inspect MakePath.Repo.one(q)
                   {freq, timestamp + freq, count + 1}
                 else
                   {freq, next, count + 1}
                 end
               end)
    {:ok, agent_pid}
  end
end
