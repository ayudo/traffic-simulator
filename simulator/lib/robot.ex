defmodule Robot do
  use GenServer

  @event_manager ClockManager
  @event_module Forwarder
  defmodule State do
    defstruct freq: 10, device_state: nil
  end


  ## Public API

  def start_link(opts \\ []) do
    {:ok, agent_pid} = Agent.start_link fn -> {10, nil, 0} end
    GenEvent.add_handler(@event_manager, {@event_module, agent_pid}, agent_pid)
    GenServer.start(__MODULE__, agent_pid, opts)
  end

  def get_agent_state(pid) do
    GenServer.call(pid, :get_agent_state)
  end

  def update_freq(pid, freq) do
    GenServer.cast(pid, {:update_freq, freq})
  end
  def get_agent(pid) do
    GenServer.call(pid, :get_agent)
  end
  def stop(pid) do
    agent_pid = get_agent(pid)
    Agent.stop(agent_pid)
    Process.exit pid, :normal
  end
  ## Callbacks

  def init(agent_pid) do
    {:ok, %State{device_state: agent_pid}}
  end

  def handle_cast({:update_freq, freq}, state) do
    Agent.cast(state.device_state, fn({_, _, count}) -> {freq, nil, count} end)
    {
      :noreply,
      %{ state | freq: freq }
    }
  end

  def handle_call(:get_freq, _from, state) do
    {:reply, state.freq, state}
  end

  def handle_call(:get_agent, _from, state) do
    {:reply, state.device_state, state}
  end

  def handle_call(:get_agent_state, _from, state) do
    {:reply, Agent.get(state.device_state, &(&1)), state}
  end

end
