defmodule TickServer do
  use GenServer

  @event_manager ClockManager
  @heart_beat 1000

  ## API

  def start_link(freq \\ @heart_beat, opts \\ []) do
    {:ok, _manager_pid} = GenEvent.start_link([name: @event_manager])
    GenServer.start_link(__MODULE__, freq, opts)
  end


  ## Callbacks

  def init(state) do
    :timer.send_interval(1000, self, :tick)
    {:ok, state}
  end

  def handle_info(:tick, state) do
    GenEvent.notify(@event_manager, {:tick, timestamp})
    {:noreply, state}
  end

  def timestamp() do
    {mega, sec, _micro} = :erlang.now
    mega * 1_000_000 + sec
  end    
end

