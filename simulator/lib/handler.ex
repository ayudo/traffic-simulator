defmodule Handler do
  use GenEvent
  def init(pid) do
    IO.puts "init"
    IO.inspect pid
    {:ok, pid}
  end
  def handle_event(event, pid) do
    IO.inspect pid
    send pid, event
    {:ok, pid}
  end
end
