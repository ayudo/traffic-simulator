use Mix.Config

config :simulator, Simulator.Repo,
  database: "ayudo",
  username: "david",
  password: "testdb",
  hostname: "localhost"

config :logger, compile_time_purge_level: :info
