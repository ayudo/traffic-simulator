defmodule MapScraperUtilsTest do
  use ExUnit.Case, async: true

  import Utils

  defp same_list?(l1, l2) do
    length(l1) == length(l2) and :lists.zipwith(&(&1==&2), l1, l2) |> Enum.all?
  end

  doctest Utils, except: [take_random: 2]

  test "Reasonnable randomness" do
    not_seeded = for _ <- 1..10 do
      :random.seed 0, 0, 0
      :random.seed 0, 0, 0
      1..100 |> Enum.shuffle
             |> Enum.take 4
    end
    seeded = for _ <- 1..10 do
      :random.seed 0, 0, 0
      :random.seed 0, 0, 0
      take_random(1..100, 4)
    end
    assert Enum.all?( not_seeded, &(same_list? &1, hd(not_seeded)) ) == true
    assert Enum.all?( seeded, &(same_list? &1, hd(seeded)) ) == false
  end

  test "Ordered pairs" do
    :random.seed :erlang.now
    l = 1..10 |> Enum.to_list
    result1 = for a <- l, b <- l -- [a], do: {a, b}
    result2 = pairs(l) |> Enum.sort
    assert same_list?(result1, result2)
  end

end
