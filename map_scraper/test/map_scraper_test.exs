defmodule MapScraperTest do
  use ExUnit.Case, async: true

  import MapScraper

  test "JSON success (test could fail if connectivity error)" do
    assert get_road({"Paris", "Marseille"}) == {:ok, 'paris---marseille.json'}
    assert File.exists? './priv/gmaps_cache/paris---marseille.json'
    :ok = File.rm './priv/gmaps_cache/paris---marseille.json'
  end

  test "invalid directions (test could fail if connectivity error)" do
    assert get_road({"Melmak", "Andromeda"}) == {:fail, {:zero_results, 'melmak---andromeda.json'}}
    assert File.exists? './priv/gmaps_cache/trash/melmak---andromeda.json.fail'
    :ok = File.rm './priv/gmaps_cache/trash/melmak---andromeda.json.fail'
  end

end
