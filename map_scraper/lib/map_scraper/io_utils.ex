defmodule IOUtils do

  @moduledoc """
  Helper to read/write files.
  """

  @doc """
  Check if a file is valid JSON from Google Directions API.
  """
  @spec check_file(binary, {MapScraper.filename, Path.t}, Path.t)
                  :: {:ok, MapScraper.filename}
                   | {:fail, {MapScraper.api_status, MapScraper.filename}}

  def check_file(content, {filename, _filepath} = fileinfo, trash) do
    case(Json.check_json content) do
      :ok ->
        {:ok, filename}
      status ->
        :ok = trash_file fileinfo, trash
        {:fail, {status, filename}}
    end
  end

  @doc """
  Move an invalid JSON file to a trash directory for further
  inspection.
  """
  @spec trash_file({MapScraper.filename, Path.t}, Path.t)
                  :: :ok
                   | {:error, File.posix}
                   | {:error, :badarg}
  def trash_file({filename, filepath} = _fileinfo, trash) do
    trashed = trash ++ filename ++ '.fail'
    :file.rename(filepath, trashed)
  end

  @doc """
  Check if a directory exists, create it if it doesn't.
  """
  @spec check_dir(Path.t) :: :ok | {:error, File.posix}
  def check_dir(dir) do
    case File.dir? dir do
      true  -> :ok
      false -> File.mkdir_p dir
    end
  end

  @doc """
  Return the content of a file as a list of its lines.
  """
  @spec file_to_list(Path.t)  :: {:ok, [binary]}
                               | {:error, :file.posix}   
  def file_to_list filepath  do
    case File.read filepath  do
      {:ok, content} ->
        list = content |> String.split("\n", trim: true)
                       |> to_char_list
        {:ok, list}
      {:error, reason} -> {:error, reason}
    end
  end

  @doc """
  Make an appropriate filename from a 2-tuple of places.
  """
  @spec make_filename(MapScraper.road) :: binary  
  def make_filename({orig, dest} = _road) do
    sanitize(orig) ++ '---' ++ sanitize(dest) ++ '.json'
  end

  @spec sanitize(String.t) :: binary
  defp sanitize( name ) do
    name |> String.strip
         |> String.downcase
         |> String.replace(~r/'/, "_")
         |> String.replace(~r/ *, */, "__")
         |> String.replace( ~r/ +/, "_")
         |> to_char_list
  end
end
