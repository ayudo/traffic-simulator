defmodule Json do

  require Poison

  @moduledoc """
  JSON decoding. Depends of [Poison](https://github.com/devinus/poison).

  ## Status Codes

  The status field within the Directions response object contains the
  status of the request, and may contain debugging information to help
  you track down why the Directions service failed. The status field may
  contain the following values:

  * OK indicates the response contains a valid result.
  * NOT_FOUND indicates at least one of the locations specified in the
  request's origin, destination, or waypoints could not be geocoded.
  * ZERO_RESULTS indicates no route could be found between the origin
  and destination.
  * MAX_WAYPOINTS_EXCEEDED indicates that too many waypointss were
  provided in the request The maximum allowed waypoints is 8, plus the
  origin, and destination. ( Google Maps API for Work customers may
  contain requests with up to 23 waypoints.)
  * INVALID_REQUEST indicates that the provided request was invalid.
  Common causes of this status include an invalid parameter or
  parameter value.
  * OVER_QUERY_LIMIT indicates the service has received too many
  requests from your application within the allowed time period.
  * REQUEST_DENIED indicates that the service denied use of the
  directions service by your application.
  * UNKNOWN_ERROR indicates a directions request could not be processed
  due to a server error. The request may succeed if you try again.
  """

  @doc """
  Check if a binary is valid JSON.
  """
  @spec check_json(binary) :: MapScraper.api_status
                            | :invalid_json
  def check_json(json) do
    case Poison.decode(json) do
      {:error, _invalid} -> :invalid_json
      {:ok, data}        -> get_status(data)
    end
  end

  @doc """
  Decode JSON and return the value of the `status` attribute.
  """
  @spec get_status(term) :: MapScraper.api_status
  def get_status(data) do
    case data["status"] do
      "OK"               -> :ok
      "NOT_FOUND"        -> :not_found
      "ZERO_RESULTS"     -> :zero_results
      "INVALID_REQUEST"  -> :invalid_request
      "OVER_QUERY_LIMIT" -> :over_query_limit
      "REQUEST_DENIED"   -> :request_denied
      "UNKNOWN_ERROR"    -> :unknown_error
      _invalid_json      -> :invalid_json
    end
  end
end
