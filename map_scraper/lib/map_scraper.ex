defmodule MapScraper do

  require Logger
  import IOUtils
  import HttpUtils

  @cache_dir      './priv/gmaps_cache/'
  @trash_dir      './priv/gmaps_cache/trash/'
  @places_file     './priv/french_towns.txt'

  @api_key        "AIzaSyB3u3HVW6eWk4ES1vioJhLapFt_y6FGA4o"
  @directions_api "https://maps.googleapis.com/maps/api/directions/json?"
  @max_retry      3
  @api_limit      2000
  @delay          1500


  @type filename    :: binary
  @type road        :: [{origin, destination}]
  @type origin      :: place
  @type destination :: place
  @type place       :: String.t
  @type status      :: {:ok, filename}
                     | {:fail, {api_status, filename}}
                     | {:error, :time_out}
  @type api_status  :: :ok
                     | :not_found
                     | :zero_results
                     | :invalid_request
                     | :over_query_limit
                     | :request_denied
                     | :unknown_error
                     | :invalid_json

  @moduledoc """
  This module allows batch-querying of Google Directions API.
  
  A list of paths is generated from a file containing a list of places
  separated by carriage returns. That file is located at a path defined
  in the module attribute `@places_file`.
  
  JSON objects returned by Google Directions API are stored in the
  directory defined in the module attribute `@cache_dir`.

  ## Usage Limit

  * 2,500 directions requests per 24 hour period.
  * 2 requests per second. 

  See the [API Documentation](https://developers.google.com/maps/documentation/directions/)
  """

  @doc """
  Get `n` directions from Google Directions API and store the result in
  `@cache_dir`. Directions are choosen from a list of places stored in
  `@places_file`
  """
  @spec cache_directions(integer) :: [status]
  def cache_directions(n) when n < @api_limit do
    check_dir @cache_dir
    check_dir @trash_dir
    roads = random_roads(n)
    scrape_roads roads
  end

  @doc """
  Get directions for a list of path from Google Directions API.
  """
  @spec scrape_roads([road]) :: [status]
  def scrape_roads(roads), do: scrape_roads(roads, 0, [])
  defp scrape_roads([], _, acc), do: acc
  defp scrape_roads([x|xs], delay, acc) do
    :timer.sleep delay
    scrape_roads xs, @delay, [get_road(x)|acc]
  end

  @doc """
  Get directions between 2 places from Google Directions API.
  """
  @spec get_road(road) :: status
  def get_road(road) do
    filename = road |> make_filename
    IO.inspect filename
    filepath = @cache_dir ++ filename
    url      = road |> make_url {@directions_api, @api_key, "driving"}
    get_road road, {filename, filepath}, url
  end
 
  defp get_road(road, fileinfo, url) do
    get_road(road, fileinfo, url, @max_retry)
  end
  defp get_road(road, _, _, 0), do: {:error, :time_out} |> log road
  defp get_road(road, {_filename, filepath} = fileinfo, url, retry) do
    case File.read filepath do
      {:ok, content} ->
        check_file(content, fileinfo, @trash_dir) |> log road
      _nocache ->
        from = self
        {:ok, ref} = query url, filepath, &(send from, &1)
        receive do
          {^ref, :saved_to_file}    -> remaining = retry
          {^ref, {:error, _reason}} -> remaining = retry - 1
          {^ref, _bogus}            -> remaining = 0
          _unexpected               -> remaining = retry
        after
          2000                      -> remaining = retry - 1
        end
        get_road road, fileinfo, url, remaining
    end
  end

  @doc """
  Randomly select `n` paths among all possible paths in a list of
  places.
  """
  @spec random_roads(integer, Path.t) :: [road]
  def random_roads(n, filename \\ @places_file) do
    filename |> all_roads
             |> Utils.take_random n
  end

  @doc """
  Get all possible paths from a list of places. Be careful with this,
  if `n` is the number of places, there are `n * (n - 1) paths.
  """
  @spec all_roads(Path.t) :: [road]
  def all_roads(filename \\ @places_file) do
    {:ok, list} = IOUtils.file_to_list(filename)
    list |> Utils.pairs
  end

  defp log(result, {orig, dest}) do
    case result do
      {:ok, filename} ->
        Logger.info "Saved directions \'#{orig} - #{dest}\' "
                    <> "in \'#{@cache_dir}#{filename}\'."
      {:error, _ } ->
        Logger.warn "Can't query directions \'#{orig} - #{dest}\'."
      {:fail, {status, filename}} -> 
        Logger.warn "Failed to get directions \'#{orig} - #{dest}\' "
                    <> "error status: \'#{status}\'. File moved to "
                    <> "\'#{@trash_dir}#{filename}.fail\'."
    end
    result
  end
end
