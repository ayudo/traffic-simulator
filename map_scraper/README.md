MapScraper
==========

This module allows batch-querying of Google Directions API.

A list of paths is generated from a file containing a list of places
separated by carriage returns. That file is located at a path defined
in the module attribute `@places_file`.

JSON objects returned by Google Directions API are stored in the
directory defined in the module attribute `@cache_dir`.


## Usage Limit

* 2,500 directions requests per 24 hour period.
* 2 requests per second. 


See the [API Documentation](https://developers.google.com/maps/documentation/directions/)
