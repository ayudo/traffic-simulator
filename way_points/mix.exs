defmodule WayPoints.Mixfile do
  use Mix.Project

  def project do
    [app: :way_points,
     version: "0.0.1",
     elixir: "~> 1.0",
     deps: deps]
  end

  def application do
    [applications: [:logger, :postgrex, :ecto]]
  end

  defp deps do
    [   
      {:poison, git: "https://github.com/devinus/poison.git"},
      {:polyline, git: "https://github.com/dvionnet/polyline.git"},
      {:ecto, "~> 0.5"},
      {:postgrex, "~> 0.5"}, 
      {:earmark, "~> 0.1", only: :dev},
      {:ex_doc, "~> 0.6", only: :dev},
    ]  
  end
end
