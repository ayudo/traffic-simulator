defmodule MakePath.Repo.Migrations.CreateSteps do
  use Ecto.Migration

  def up do
    create table(:steps) do
     add :second,     :integer
     add :lat,      :float
     add :lng,      :float
     add :speed,    :float
     add :movement, :string, size: 40
     add :ref, :string, size: 40
     add :cell_id, :string, size: 40
    end
  end

  def down do
    drop table(:steps)
  end
end
