defmodule HttpUtils do

  @moduledoc """
  HTTP helper, use the `inets` service API.
  """

  @doc """
  Returns an URL in a format suitable to query Google Maps API.
  Parameters are formatted as query string.
  """
  @spec make_url(MapScraper.road, {String.t, String.t, String.t}) :: binary
  def make_url({origin, destination}, {api_url, key, mode}) do
    query = %{ origin:      origin,
               destination: destination,
               mode:        mode,
               key:         key }
    q_string = URI.encode_query query
    (api_url <> q_string) |> to_char_list
  end

  @doc """
  Get a ressource at the specified location and stream the result to
  the specified file.
  """
  @spec query(String.t, Path.t, (tuple -> tuple)) :: {:ok, reference}
                                                   | {:error, binary}
  def query(url, filepath, receiver) do
    method = :get
    header = []
    http_options = [ timeout: 2000, ssl: [verify: 0] ]
    options = [ sync: false, stream: filepath, receiver: receiver ]
    :httpc.request method, {url, header}, http_options, options
  end

end
