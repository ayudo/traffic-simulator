defmodule WayPoints do

  @doc """
  Takes a JSON file from Google Directions API and returns a list of
  coordinates for those directions, one waypoint per second traveled.
  """
  @spec generate(Path.t) :: [Navigation.point]
  def generate(filepath \\ 'agen__france---avignon__france.json') do
    data = get_data(filepath)
    start_point = get_start(data)
    points = data |> get_paths
                  |> Enum.flat_map &(tl(cut_intervals &1))
    [{start_point, 0.0}|points]
  end

  @doc """
  Decode a JSON file.
  """
  @spec get_data(Path.t) :: Map.t
  def get_data(filepath) do
    filepath |> File.read
             |> unwrap
             |> Poison.decode
             |> unwrap
  end

  defp get_legs(data), do: hd(data["routes"])["legs"]
  defp get_start(data) do
    loc = hd(get_legs data)["start_location"]
    { loc["lat"], loc["lng"] }
  end
  defp get_steps(data), do: hd(get_legs data)["steps"]
  defp get_paths(data) do
    {paths, _} =  data |> get_steps
                       |> Enum.map_reduce(0.0, &format_subpath/2)
    paths
  end

  defp format_subpath(step, time_offset) do
    duration = step["duration"]["value"]
    points = point_list step["polyline"]["points"]
    { {time_offset, duration, points}, duration + time_offset }
  end

  defp point_list(polyline) do
    polyline |> to_char_list
             |> Polyline.to_point_list
             |> unwrap

  end

  ## pair of points and interval
  defp segment({point_a, point_b}, {time_a, time_b}) do
    n = int_between(time_a, time_b)
    delta = time_b - time_a
    first_offset = Float.ceil(time_a) - time_a
    segment(point_a, point_b, n - 1, delta, first_offset)
  end

  defp segment(_, _, -1, _, _), do: []
  defp segment(a, b, n, delta, offset) do
    for i <- 0..n, do: Navigation.interpolate(a, b, (offset + i) / delta)
  end

  defp int_between(x, y) do
    y = Float.round(y, 5) # 4.99999 = 5, previous rounding errors
    Float.floor(y) - Float.ceil(x) + 1 |> Kernel.round
  end

  defp cut_intervals({offset, duration, points}) do
    pairs = pair_elements points
    distances = Enum.map pairs, &Navigation.distance/1
    speed = get_speed distances, duration
    times = distances |> Enum.map(&(&1/speed))
    t_intervals = [offset|Enum.scan(times, offset, &(&1 + &2))]
                  |> pair_elements
    :lists.zipwith(&(segment &1, &2), pairs, t_intervals) |> List.flatten
                                                          |> Enum.map(&( {&1, speed}))
  end 

  
  defp get_speed(distances, duration), do: Enum.sum(distances) / duration

  def add_bearings(points), do: add_bearings(Enum.zip(points, tl(points)), [])
  def add_bearings([{{a, _}, {b, _}}|[]], acc) do
    Enum.reverse([Navigation.final_bearing(a, b), Navigation.bearing(a, b)| acc])
  end
  def add_bearings([{{a,_}, {b,_}}|xs], acc) do
    add_bearings(xs, [Navigation.bearing(a, b)|acc])
  end

  def to_csv(points, filename) do
    {:ok, pid} = File.open(filename, [:write])
    IO.puts(pid, "x,y,description")
    to_csv(points, pid, 0)
    File.close pid
  end
  defp to_csv([], _, count), do: count
  defp to_csv([x|xs], pid, count) do
     IO.puts(pid, "#{elem(x,0)},#{elem(x,1)},#{count}")
     to_csv(xs, pid, count + 1)
  end

  ### Utils

  @doc """
  Unwrap a tuple returned as a result. In case a dependency on
  `elixir-pipes` is overkill.
  """
  @spec unwrap({:ok, term}) :: term
  def unwrap({:ok, result}), do: result

  @doc """
  Pair every element of a list with its successor.
  """
  @spec pair_elements(Enumerable.t) :: [{Enumerable.element, Enumerable.element}]
  def pair_elements([]), do: []
  def pair_elements(l), do: Enum.zip(l, tl l)

end
