defmodule Navigation do
  

  import :math
  @mean_radius 6_371_000 #earth radius, in meters

  @type arc        :: {point, point}
  @type point      :: {angle_deg, angle_deg}
  @type angle_deg  :: float
  @type angle_rad  :: float
  @type distance   :: float
 
  @doc """
  Convert an angle in degrees to an angle in radians.
  """
  @spec rad(angle_deg) :: angle_rad
  def rad(theta) when is_number(theta), do: theta * pi / 180

  @spec rad(point) :: {angle_rad, angle_rad}
  def rad({lat, lon}), do: {rad(lat), rad(lon)}

  @doc """
  Convert an angle in radians to an angle in degrees.
  """
  @spec deg(angle_rad) :: angle_deg
  def deg(theta) when is_number(theta), do: theta / pi * 180

  @spec deg({angle_rad, angle_rad}) :: point
  def deg({lat, lon}), do: {deg(lat), deg(lon)}

  @doc """
  The haversine function:
  
  `haversin(θ) = sin²( θ / 2)`
  
  """
  @spec haversin(angle_rad) :: float
  def haversin(theta), do: pow(sin(theta / 2), 2)

  @doc """
  Return the central angle between two points.

  `a = haversin Δφ + cos φ1 ⋅ cos φ2 + haversin Δλ`
  `c = 2 ⋅ atan2( √a, √(1−a) )`
  
  """
  @spec central_angle(point, point) :: angle_rad
  def central_angle(a, b) do
    {phi_1, lambda_1} = rad a
    {phi_2, lambda_2} = rad b
    d_phi = phi_2 - phi_1
    d_lambda = lambda_2 - lambda_1
    a = haversin(d_phi) + cos(phi_1) * cos(phi_2) * haversin(d_lambda)
    2 * atan2(sqrt(a), sqrt(1 - a))
  end

  @doc """
  Return the distance between two points on Earth.

  `a = haversin Δφ + cos φ1 ⋅ cos φ2 + haversin Δλ`
  `c = 2 ⋅ atan2( √a, √(1−a) )`
  `d = R ⋅ c`

  """
  @spec distance(arc) :: distance
  def distance({a, b}), do: central_angle(a, b) * @mean_radius

  @doc """
  Return the point at a given fraction of the distance between 2 points
  on a great circle. `interpolate(a, b, 0)` is the point `a` and
  `interpolate(a, b, 1)` is the point `b`.

  """
  @spec interpolate(point, point, float) :: point
  def interpolate(point_a, point_b, fraction) do
    {phi_1, lambda_1} = rad(point_a)
    {phi_2, lambda_2} = rad(point_b)
    theta = central_angle(point_a, point_b) 
    a = sin((1 - fraction) * theta) / sin(theta)
    b = sin(fraction * theta) / sin(theta)
    x = a * cos(phi_1) * cos(lambda_1) + b * cos(phi_2) * cos(lambda_2)
    y = a * cos(phi_1) * sin(lambda_1) + b * cos(phi_2) * sin(lambda_2)
    z = a * sin(phi_1) + b * sin(phi_2)
    lat = atan2(z, sqrt(x * x + y * y))
    lon = atan2(y, x)
    deg {lat, lon}
  end

  @doc """
  Calculate the bearing (or azimuth) of an object at point `a` when
  heading to point `b`.

  `θ = atan2( sin Δλ ⋅ cos φ2 , cos φ1 ⋅ sin φ2 − sin φ1 ⋅ cos φ2 ⋅ cos Δλ )`

  """
  @spec bearing(point, point) :: angle_deg
  def bearing(a, b) do
    {phi_1, lambda_1} = rad a
    {phi_2, lambda_2} = rad b
    d_lambda = lambda_2 - lambda_1
    az = atan2(sin(d_lambda) * cos(phi_2),
               cos(phi_1) * sin(phi_2) - sin(phi_1) * cos(phi_2) * cos(d_lambda))
    IO.inspect (deg az)
    normalize(deg az)
  end

  def normalize(theta) when theta < 0 do
    IO.inspect 360 + theta
    normalize(360 + theta)
  end
  def normalize(theta) when theta >= 0 do
    int_part = :erlang.trunc theta
    mantissa = theta - int_part
    rem(int_part, 360) + mantissa
  end

  def inverse(theta), do: normalize(180 + theta)

  def final_bearing(a, b), do: inverse bearing(b, a)


  def bearings(points), do: bearings(Enum.zip(points, tl(points)), [])
  def bearings([{a, b}|[]], acc) do
    Enum.reverse([final_bearing(a, b), bearing(a, b)| acc])
  end
  def bearings([{a, b}|xs], acc) do
    bearings(xs, [bearing(a, b)|acc])
  end


end
