defmodule Utils do
  @moduledoc """
  Common utilities.
  """

  @doc """
  Return all ordered pair permutations of elements from a list. Erlang
  books would show something idiomatic like this:

      def pairs(l), do: for a <- l, b <- l -- [a], do: {a, b}

  ... very elegant, but O(n^3) in time and memory when this problem
  really should be O(n^2). This is a slightly more efficient solution.

  The returned list of pairs is _not_ ordered.

  ## Examples

      iex> Utils.pairs([1, 2, 3])
      [{3, 1}, {1, 3}, {3, 2}, {2, 3}, {2, 1}, {1, 2}]

  """
  @spec pairs(Enumerable.t) :: [{any, any}]
  def pairs(list), do: pairs(list, [], [])
  defp pairs([], _nodes, acc), do: acc
  defp pairs([x|xs], nodes, acc), do: pairs(xs, [x|nodes], accumulate_pairs(x, nodes, acc))
  defp accumulate_pairs(_e, [], acc), do: acc
  defp accumulate_pairs(e, [n|ns], acc), do: accumulate_pairs(e, ns, [{e, n} | [{n, e}|acc]])

  @doc """
  Combine two lists of different size into one list. For each pair
  `x, y` of list elements from the two lists, the element in the result
  list will be `fun(x, y)`. If one of the two lists is exhausted, that
  list's element is replaced with `ident`, the neutral element for the
  `fun` operator.

  ## Examples
  
      iex> Utils.combine([1,2,3], [4,5], &(&1 * &2), 1)
      [4, 10, 3]

      iex> Utils.combine([7,8,9], [2,3], &(&1 + &2), 0)
      [9, 11, 9]

      iex> Utils.combine([:true, :false, :true], [:true, :false], &(&1 && &2), :true)
      [true, false, true]

      iex> Utils.combine([:name, :mail], ["sam"], fn(a,b) -> {a,b} end, nil)
      [name: "sam", mail: nil]

      iex> Utils.combine([1, 2, 3], [4, 5], &(Enum.join([&1,&2])), []) 
      ["14", "25", "3"]

  """
  @spec combine(Enumerable.t, Enumerable.t, ((any, any) -> any), any) :: Enumerable.t
  def combine(list1, list2, fun, ident) do
    combine(list1, list2, fun, ident, [])
  end
  defp combine([], [], _fun, _ident, acc), do: Enum.reverse(acc)
  defp combine([x|xs], [y|ys], fun, ident, acc) do
    combine(xs, ys, fun, ident, [fun.(x, y)|acc])
  end
  defp combine([], [y|ys], fun, ident, acc) do
    combine([], ys, fun, ident, [fun.(ident, y)|acc])
  end
  defp combine([x|xs], [], fun, ident, acc) do
    combine(xs, [], fun, ident, [fun.(x, ident)|acc])
  end

  @doc """
  Randomly select n elements of a list
  
      iex> Utils.take_random([1, 2, 3, 4], 2)
      [4, 1]

  """
  @spec take_random(Enumerable.t, integer) :: Enumerable.t
  def take_random(list, n) do
    :random.seed :erlang.now
    list |> Enum.shuffle
         |> Enum.take n
  end

  defmodule Benchmark do
    @moduledoc """
    Minimal benchmark tool.
    """
    @doc """
    Return some basic stats about execution time.
    """
    def test_avg({m, f, a}, n) do
      exec_times = test_loop({m, f, a}, n, [])
      len = length exec_times
      min = Enum.min(exec_times)
      max = Enum.max(exec_times)
      median = Enum.at(Enum.sort(exec_times), round(len/2))
      avg = round( Enum.sum(exec_times) / len )
      IO.puts "
      Range  : #{min} - #{max}
      Median : #{median}
      Average: #{avg}"
    end

    defp test_loop(_fun, 0, exec_times), do: exec_times
    defp test_loop({m, f, a} = fun, n, exec_times) do
      {time, _value} = :timer.tc(m, f, a)
      test_loop(fun, n - 1, [time | exec_times])
    end
  end

end

