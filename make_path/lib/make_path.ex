defmodule MakePath.App do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec
    tree = [worker(MakePath.Repo, [])]
    opts = [name: MakePath.Sup, strategy: :one_for_one]
    Supervisor.start_link(tree, opts)
  end
end

defmodule MakePath.Repo do
  use Ecto.Repo,
    otp_app: :simulator,
    adapter: Ecto.Adapters.Postgres
end

defmodule Step do
  use Ecto.Model

  schema "steps" do
    field :second, :integer
    field :lat, :float
    field :lng, :float
    field :speed, :float
    field :movement, :string
    field :ref, :string
  end
end

defmodule MakePath do
  import Ecto.Query

  def record_path(waypoints, ref), do: record_path(waypoints, ref, 0)
  def record_path([{{lat, lng}, speed}], ref, n) do
    MakePath.Repo.insert(%Step{second: n,
                             lat: lat,
                             lng: lng,
                             speed: speed,
                             movement: "arret",
                             ref: ref})
  end
  def record_path([{{lat, lng}, speed}|tail], ref, 0) do
    MakePath.Repo.insert(%Step{second: 0,
                             lat: lat,
                             lng: lng,
                             speed: speed,
                             movement: "demarrage",
                             ref: ref})
    record_path(tail, ref, 1)
  end
  def record_path([{{lat, lng}, speed}|tail], ref, n) do
    MakePath.Repo.insert(%Step{second: n,
                             lat: lat,
                             lng: lng,
                             speed: speed,
                             movement: "roule",
                             ref: ref})
   record_path(tail, ref, n + 1)
  end

  def from_cache do
    {:ok, json_files} = File.ls "priv/gmaps_cache"
    process_files(json_files)
  end

  def process_files(files), do: process_files(files, [])
  def process_files([], acc), do: acc
  def process_files([x|xs], acc) do
    ref = :erlang.make_ref
          |> :erlang.ref_to_list
          |> to_string
    waypoints = WayPoints.generate "priv/gmaps_cache/" <> x
    record_path(waypoints, ref)
    process_files(xs, [ref|acc])
  end

end

